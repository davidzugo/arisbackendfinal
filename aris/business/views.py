# from django.shortcuts import render
from django.http.response import JsonResponse
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from django.views import View
from .models import Negocio
import json

# Create your views here.

class NegocioView(View):
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super().dispatch(request, *args, **kwargs)

    def get(self,request,id=0):
        if(id>0):
            negocios=list(Negocio.objects.filter(id=id).values())
            if len(negocios)>0:
                    negocios=negocios[0]
                    datos={'message':"Negocios no encontrados",'negocios':negocios}
            else:
                    datos={'message':"Negocios no encontrados"}
            return JsonResponse(datos)    
        else:   
            negocios=list(Negocio.objects.values())
            if len(negocios)>0:
                datos={'message':"Success",'negocios':negocios}
            else:
                datos={'message':"Negocios no encontrados listando a todos "}
            return JsonResponse(datos)

    def post(self,request):
            # print(request.body)
            jd=json.loads(request.body)
            Negocio.objects.create(id_usuario=jd['id_usuario'],nombre=jd['nombre'],telefono=jd['telefono'],direccion=jd['direccion'],horario=jd['horario'],descripcion=jd['descripcion'],calificacion=jd['calificacion'])
            datos={'message':"Success"}
            return JsonResponse(datos)
    def put(self,request,id):
        jd=json.loads(request.body)
        negocios=list(Negocio.objects.filter(id=id).values())
        if len(negocios)>0:
            negocios=Negocio.objects.get(id=id)
            negocios.nombre=jd['nombre']
            negocios.telefono=jd['telefono']
            negocios.direccion=jd['direccion']
            negocios.horario=jd['horario']
            negocios.descripcion=jd['descripcion']
            negocios.calificacion=jd['calificacion']
            negocios.save()
            datos={'message':"Success"}
            
        else:
            datos={'message':"Negocios no encontrados"}

        return JsonResponse(datos)


    def delete(self,request,id):
        negocios=list(Negocio.objects.filter(id=id).values())
        if len(negocios)>0:
            Negocio.objects.filter(id=id).delete()
            datos={'message':"Eliminado Correctamente"}


        else:
            datos={'message':"Negocios no encontrados"}
        return JsonResponse(datos)

