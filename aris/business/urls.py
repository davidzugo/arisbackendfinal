from django.urls import path
from .views import NegocioView

urlpatterns=[

    path('negocios/',NegocioView.as_view(), name='negocio_lista'),
    path('negocios/<int:id>',NegocioView.as_view(), name='negocios_process')
]