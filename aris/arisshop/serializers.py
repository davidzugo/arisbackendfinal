from rest_framework import serializers
from django.contrib.auth import password_validation, authenticate
from rest_framework.validators import UniqueValidator
from rest_framework.authtoken.models import Token
from .models import *
from django.contrib.auth.models import User

class UserSerializer(serializers.ModelSerializer):
	class Meta:
		model = User
		fields = (
			'username',
			'first_name',
			'last_name',
			'email',
			'password',
			'date_joined',
			'is_active',
			'is_staff'
		)

class LoginSerializer(serializers.Serializer):
	# Requeridos
	username = serializers.CharField()
	password = serializers.CharField(min_length=8, max_length=64)

	# Función validación
	def validate(self, data):
		# authenticate recibe las credenciales, si son válidas devuelve el objeto del usuario
		user = authenticate(username=data['username'], password=data['password'])
		if not user:
			raise serializers.ValidationError('Las credenciales no son válidas')
		if not user.is_active:
			raise serializers.AuthenticationFailed('La cuenta de usuario está desactivada.')
		# Guardamos el usuario en el contexto para posteriormente en create recuperar el token
		self.context['usuario'] = user
		return data

	def create(self, data):
		# Generar o recuperar token único
		token, created = Token.objects.get_or_create(user=self.context['usuario'])
		return self.context['usuario'], token.key

class RegistroSerializer(serializers.Serializer):
	# Parametros requeridos
	username = serializers.CharField(min_length=4)
	password = serializers.CharField(min_length=8, max_length=64)
	password_confirmation = serializers.CharField(min_length=8, max_length=64)
	email = serializers.EmailField()
	first_name = serializers.CharField()
	last_name = serializers.CharField()
	is_active = serializers.BooleanField()
	is_staff = serializers.BooleanField()

	def validate(self, data):
		passwd = data['password']
		passwd_conf = data['password_confirmation']
		if passwd != passwd_conf:
			raise serializers.ValidationError("Las contraseñas no coinciden")
		password_validation.validate_password(passwd)

		return data

	def create(self, data):
		data.pop('password_confirmation')
		user = User.objects.create_user(
			data['username'],
			data['email'],
			data['password'],
		)
		user.last_name = data['last_name']
		user.first_name = data['first_name']
		user.is_active = data['is_active']
		user.is_staff = data['is_staff']
		user.save()

		return user