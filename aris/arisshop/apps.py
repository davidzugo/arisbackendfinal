from django.apps import AppConfig


class ArisshopConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'arisshop'
