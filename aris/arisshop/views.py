from rest_framework.decorators import api_view, permission_classes
from rest_framework.permissions import AllowAny, IsAuthenticated
from rest_framework import status
from rest_framework.response import Response

from .serializers import LoginSerializer, UserSerializer, RegistroSerializer

@api_view(["POST"])
@permission_classes((AllowAny,))
def login(request):
    # Inicio de sesión
    serializer = LoginSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    user, token = serializer.save()
    data = {
        'user': UserSerializer(user).data,
        'access_token': token
    }
    return Response(data, status=status.HTTP_201_CREATED)

@api_view(["POST"])
@permission_classes((AllowAny,))
def reset_password(request, id_user):
    # TODO - Envia mail a user.email -> mail contiene url + token -> valida token -> pide nuevo pass -> elimina token antiguo
    # Inicio de sesión
    serializer = LoginSerializer(data=request.data)
    serializer.is_valid(raise_exception=True)
    user, token = serializer.save()
    data = {
        'user': UserSerializer(user).data,
        'access_token': token
    }
    return Response(data, status=status.HTTP_201_CREATED)

@api_view(["POST"])
@permission_classes((IsAuthenticated,))
def logout_all(request):
    # Cerrar sesión en todos los dispositivos
    try:
        request.user.auth_token.delete()
    except (AttributeError):
        pass
    return Response(status=status.HTTP_200_OK)

@api_view(["POST"])
@permission_classes((AllowAny,))
def register(request):
    # Registro de usuarios de API
	serializer = RegistroSerializer(data = request.data)
	serializer.is_valid(raise_exception=True)
	user = serializer.save()
	data_returned = UserSerializer(user).data
	return Response(data_returned, status=status.HTTP_201_CREATED)